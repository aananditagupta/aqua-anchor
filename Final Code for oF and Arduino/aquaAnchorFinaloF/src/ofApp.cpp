#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetVerticalSync(true);
    
    /*Background colour depthtest etc*/
    ofEnableDepthTest();
ofDisableAlphaBlending();
    ofBackground(0,0,0);
    /*Lights*/
    ofSetSmoothLighting(true);
     //Set overall brightness of the "world"
     ofSetGlobalAmbientColor(ofColor(230));
    /*camera position*/
    cam.setPosition(0,0,100);
    
    //bSendSerialMessage = false;
    ofSetLogLevel(OF_LOG_VERBOSE);
    
    /*Intialise serial*/
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    
    // this should be set to whatever com port your serial device is connected to.
    int baud = 9600;

    serial.setup("/dev/cu.usbmodem14201", baud); // mac osx example

    
    /*Switch for gesutre sensor to be checking*/
    registerGestures = false;
    
    /*Audio switch bool and audio initialisation*/
    //downSplash.load("splash4.wav");
    rightSplash.load("splash3.wav");
    leftSplash.load("splash2.wav");
    upDownSplash.load("splash1.wav");
    theStream.load("thestream.wav");
    theStream.setVolume(0.2);
    rightSplash.setVolume(1.0);
    leftSplash.setVolume(1.0);
    upDownSplash.setVolume(1.0);
    /*Loop the stream audio*/
    theStream.setLoop(true);
    theStream.play();
    playAudio = false;
    playSound = ' ';
    
    
    /*SETUP FOR ARTWORK*/
    
    /*Sphere setup*/
    aSphere.setRadius(50);
    aSphere.setPosition(0, 0, 0);
    /*Rotation of sphere*/
    horRot = 0;
    verRot = 0;
    
    /*Image by <a href="https://pixabay.com/users/alkemade-804941/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">alkemade</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">Pixabay</a>*/
    ofDisableArbTex();
    img.load("sea.jpg");
    tex = img.getTexture();
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    /*Rotate the sphere, verRot and horROt will be set to 1 or -1 when gesture has been sensed*/
    aSphere.rotateDeg(verRot,0,1,0);
    aSphere.rotateDeg(horRot, 1,0,0);

    /*Read data from Arduino while connection is available, sotre in variable recievedData*/
    while(serial.available())
    {

        serial.readBytes(receivedData, 10);

    }
    
    /*Raise volume of stream when user is close*/
    if(registerGestures == true)
    {
        theStream.setVolume(0.4);
    }
    
    std::cout << registerGestures << std::endl;
    

}

//--------------------------------------------------------------
void ofApp::draw(){

    /*If last line is uncommented this can be used to give on screen output of what the Arduino is sending */
    string direction = ofToString(receivedData);
    //std::cout << direction << '.' << std::endl;
    string msg;
    msg += "Gesture sensor says: ";
    msg += direction;
    ofSetColor(255);
   /* ofDrawBitmapString(msg, 200, 200);*/
    
    /*Check value being sent from arduino, and then set playSOund to match it. Those values (single letters) will control what state the audio will be in*/
    char *dirForComp = &direction[0];
    playSound = *dirForComp;
    
    /*"Switch on" the gesture checking when the user is close enough to the distance sensor*/
    if(playSound == 'g')
    {
        registerGestures = true;
    }
    
    /*Artwork*/
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    cam.begin();
    tex.bind();
    aSphere.draw();
    tex.unbind();
    cam.end();
    
    //std::cout << "PlaySound: " << playSound << std::endl;

    
    std::cout <<"PlaySound: " << *dirForComp << std::endl;
    
    /*If the registerGestures boolean has been switched to true, that means the user is near and it's time to play sounds should a gesture be registered*/
    if(registerGestures == true)
    {
        switch(playSound) {
            case 'r' :
                    rightSplash.play();
                    std::cout << "played right splash" << std::endl;
                    serial.flush(true);
                    /*Make artwork spin anti-clockwise*/
                    verRot = 1;
                    //playSound = ' ';
                   // direction = " ";
                break;
            case 'd' :
                    upDownSplash.play();
                    std::cout << "played down splash" << std::endl;
                    serial.flush(true);
                    /*Make artwork spin forwards*/
                    horRot = 1;
                    //playSound = ' ';
                    //direction = " ";
                break;
            case 'l' :
                    leftSplash.play();
                    std::cout << "played left splash" << std::endl;
                    serial.flush(true);
                    /*Make artwork spin clockwise*/
                    verRot = -1;
                    //playSound = ' ';
                    //direction = " ";
                break;
            case 'u' :
                    upDownSplash.play();
                    std::cout << "played up splash" << std::endl;
                    serial.flush(true);
                    /*Make artwork spin backwards*/
                    horRot = -1;
                    //playSound = ' ';
                    //direction = " ";
                break;
              
        }
    }

}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){
    

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){


}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}


