#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
//    serialMessage = false;
//
//    serial.listDevices();
//    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    
    // this should be set to whatever com port your serial device is connected to.
    // (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
    // arduino users check in arduino app....
//    int baud = 9600;
//    serial.setup(0, baud); //open the first device
    //serial.setup("COM10", baud); // windows example
    //serial.setup("/dev/tty.usbserial-A4001JEC", baud); // mac osx example
    //serial.setup("/dev/ttyUSB0", baud); //linux example
    
    arduino.connect("/dev/cu.usvmodem145201",57600);
    ofAddListener(arduino.EInitialized, this, &ofApp::setupArduino);
}

//--------------------------------------------------------------
void ofApp::update(){
//if (serialMessage) {
//        //serialMessage = false;
//
//        serial.writeByte(sendData); // sending the data to arduino
//
//        serial.readBytes(receivedData, 10); // Getting the data from Arduino
//        printf("receivedData is %d \n", receivedData);    // Printing in ASCII format
//    }
    
//    serial.writeByte(sendData);
    arduino.update();
    
}

//--------------------------------------------------------------
void ofApp::draw(){
//    ofBackground(0);    // Black background
//    ofSetColor(255);    // Text color is white
//
//    string msg;
//    msg += "Click to turn LED \n";
//    msg += receivedData;
//    ofDrawBitmapString(msg, 200, 200); // Write the data on the output window
//    std::cout << sendData << std::endl;
}

//--------------------------------------------------------------
void ofApp::setupArduino(const int & version)
{
    ofRemoveListener(arduino.EInitialized, this, &ofApp::setupArduino);
    arduino.sendDigitalPinMode(13, ARD_OUTPUT);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch (key) {
        case OF_KEY_UP:
            arduino.sendDigital(13, ARD_HIGH);
            break;
        
        case OF_KEY_DOWN:
            arduino.sendDigital(13, ARD_LOW);
            break;
    }
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    


//    if (serialMessage == false) {
//        //serialMessage = false;
//            serialMessage = true;
// // sending the data to arduino
//        sendData = 1;
//
//        serial.readBytes(receivedData, 10); // Getting the data from Arduino
//        printf("receivedData is %d \n", receivedData);    // Printing in ASCII format
//    }
//    else if(serialMessage)
//    {
//        serialMessage = false;
//        sendData = 0;
//    }


}



