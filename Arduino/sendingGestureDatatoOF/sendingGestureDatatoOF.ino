/*
  Software serial multple serial test

 Receives from the two software serial ports,
 sends to the hardware serial port.

 In order to listen on a software port, you call port.listen().
 When using two software serial ports, you have to switch ports
 by listen()ing on each one in turn. Pick a logical time to switch
 ports, like the end of an expected transmission, or when the
 buffer is empty. This example switches ports when there is nothing
 more to read from a port

 The circuit:
 Two devices which communicate serially are needed.
 * First serial device's TX attached to digital pin 10(RX), RX to pin 11(TX)
 * Second serial device's TX attached to digital pin 8(RX), RX to pin 9(TX)

 Note:
 Not all pins on the Mega and Mega 2560 support change interrupts,
 so only the following can be used for RX:
 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69

 Not all pins on the Leonardo support change interrupts,
 so only the following can be used for RX:
 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).

 created 18 Apr. 2011
 modified 19 March 2016
 by Tom Igoe
 based on Mikal Hart's twoPortRXExample

 This example code is in the public domain.

 */

#include <SoftwareSerial.h>
#include "Adafruit_APDS9960.h"
Adafruit_APDS9960 apds;

/*Code for distance sesnsor based on:
/* Ultrasonic Sensor HC-SR04 and Arduino Tutorial
*
* by Dejan Nedelkovski,
* www.HowToMechatronics.com - https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
*
*/
/*To first check that user is wihin range, then distance sensor is switched off*/
bool inRange = false;
// defines pins numbers
const int trigPin = 9;
const int echoPin = 10;

long duration;
int distance;

// software serial #1: RX = digital pin 10, TX = digital pin 11
SoftwareSerial portOne(10, 11);

// software serial #2: RX = digital pin 8, TX = digital pin 9
// on the Mega, use other pins instead, since 8 and 9 don't work on the Mega
SoftwareSerial portTwo(8, 9);

/*Message received from oF*/
int ofMsg = 0;

void setup() {

  /*Timer code from https://www.instructables.com/id/Arduino-Timer-Interrupts/ by amandaghassaei*/
  //cli(); /*stop timer interrupts*/

  /*set timer1 interrupt at 1Hz
 // TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);*/

/*toggles to control message being sent*/
  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

/*Setup dist sensor*/
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input

  // Start each software serial port
 // portOne.begin(9600);
  //portTwo.begin(9600);

      if(!apds.begin()){
    Serial.println("failed to initialize device! Please check your wiring.");
  }
  else Serial.println("Device initialized!");

  //gesture mode will be entered once proximity mode senses something close
  apds.enableProximity(true);
  apds.enableGesture(true);

 // sei();/*Allow timer interrutps*/
}

void loop() {
  // By default, the last intialized port is listening.
  // when you want to listen on a port, explicitly select it:


  
 /* portOne.listen();
  Serial.println("Data from port one:");
  // while there is data coming in, read it
  // and send to the hardware serial port:
  while (portOne.available() > 0) {
    char inByte = portOne.read();
    Serial.write(inByte);
  }*/
  //if(Serial.available() > 0)
 // {
    //ofMsg = Serial.read();
    //Serial.println(ofMsg);

  //  if(ofMsg > 0)
    //{
    /*Check if user is within range and then start checking for gestures*/
    if(inRange == false)
    {
        checkDistance();
    }
    else
    {
        recGestureSendData();
    }
      
        //ofMsg = 0;
    //}
 // }


  // blank line to separate data from the two ports:
  //Serial.println();

 /* // Now listen on the second port
  portTwo.listen();
  // while there is data coming in, read it
  // and send to the hardware serial port:
  Serial.println("Data from port two:");
  while (portTwo.available() > 0) {
    char inByte = portTwo.read();
    Serial.write(inByte);
  }

  // blank line to separate data from the two ports:
  Serial.println();*/
}

/*Code for distance sesnsor based on:
/* Ultrasonic Sensor HC-SR04 and Arduino Tutorial
*
* by Dejan Nedelkovski,
* www.HowToMechatronics.com - https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
*
*/
/*Function to check whether the user is in range, once the user is in range the
 * recGestureSendData() function and led lights will be active.
 */
void checkDistance() {

    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    distance= duration*0.034/2;
    /*If the user s within one meter start checking for gestures and active lights*/
    if(distance < 10)
    {
      Serial.println("g");
      Serial.println(" ");
      delay(10);
      inRange = true;
      //char myStr[2] = "g";
     // Serial.write(myStr, 1);
    }
    delay(10);
  
}

/*This function checks what gesture the user has made, and this will in turn trigger a splash sound (from OF)
 * and a different animation for the led lights.
 */
void recGestureSendData() {

   //read a gesture from the device
   uint8_t gesture = apds.readGesture();
   //if(apds.gestureValid())
   //{
     if(gesture == APDS9960_DOWN)
     {
      Serial.println("d");
      Serial.println(" ");
      //char myStr[2] = "d";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_UP)
     {
      Serial.println("u");
      Serial.println(" ");
      //char myStr[2] = "u";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_LEFT)
     {
      Serial.println("l");
      Serial.println(" ");
      //char myStr[2] = "l";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_RIGHT)
     {
      Serial.println("r");
      Serial.println(" ");
      //char myStr[2] = "r";
     // Serial.write(myStr, 1);
     }
      Serial.println(" ");
   // }
//   else
   //{
    //  Serial.println(" ");
   //}
   
}
